package com.daugherty.data;

import java.util.Map;
import java.util.Set;

public interface Ingest {
	byte[] getEntityId(String name);
	void updateEntity(byte[] entityId, Map<String, Object> values);
	
	byte[] getRelationshipId(String relationship, String source);
	Set<String> getSourceTypes();
	Map<String, byte[]> getRelationshipIdsBySource(String source);
	
	void establishRelationship(byte[] firstEntityId, byte[] secondEntityId, byte[] relationshipId, 
			boolean isBidirectional, Map<String, Object> values);
	
}
