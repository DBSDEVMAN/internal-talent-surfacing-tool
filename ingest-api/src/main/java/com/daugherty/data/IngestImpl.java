package com.daugherty.data;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.apache.commons.lang3.RandomUtils;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.fasterxml.jackson.core.util.ByteArrayBuilder;

public class IngestImpl implements Ingest {

	private static final String CALCULATED_ENTITY_ID = "CalculatedEntityId";
	private static final String ENTITY_TABLE = "Entity";
	private static final String ENTITY_KEY = "EntityId";

	private static final String CALCULATED_RELATIONSHIP_ID = "CalculatedRelationshipId";
	private static final String RELATIONSHIP_TABLE = "Relationship";
	private static final String RELATIONSHIP_KEY = "RelationshipId";
	private static final String RELATIONSHIP_RELATIONSHIP = "Relationship";
	private static final String RELATIONSHIP_SOURCE = "Source";

	//private static final String CALCULATED_LINK_ID = "CalculatedRelationshipId";
	private static final String LINK_TABLE = "Relationships";
	private static final String LINK_KEY = "RelationshipId";
	//private static final String LINK_RELATIONSHIP = "Relationship";
	//private static final String LINK_SOURCE = "Source";
	
	private Table entityTable;
	private Table relationshipTable;
	private Table linkTable;

	@Override
	public byte[] getEntityId(String name) {
		Table table = getEntityTable();

		KeyAttribute getItem = new KeyAttribute(ENTITY_KEY, name.getBytes());
		Item item = table.getItem(getItem);
		if (item != null) {
			return item.getBinary(CALCULATED_ENTITY_ID);
		} else {
			byte[] randomBytes = getRandomKey(table, ENTITY_KEY);

			AttributeValue nameValue = new AttributeValue();
			nameValue.setB(ByteBuffer.wrap(name.getBytes()));

			AttributeValue entityValue = new AttributeValue();
			entityValue.setB(ByteBuffer.wrap(randomBytes));

			PutItemSpec putCalculated = new PutItemSpec();
			putCalculated.getRequest().addExpectedEntry(ENTITY_KEY, new ExpectedAttributeValue(nameValue))
					.addExpectedEntry(CALCULATED_ENTITY_ID, new ExpectedAttributeValue(entityValue));
			// Create Calculated Entity Id Entry
			table.putItem(putCalculated);

			PutItemSpec entity = new PutItemSpec();
			entity.getRequest().addExpectedEntry(ENTITY_KEY, new ExpectedAttributeValue(entityValue));
			// Create Entity Entry
			table.putItem(entity);

			return randomBytes;
		}
	}

	@Override
	public void updateEntity(byte[] entityId, Map<String, Object> values) {
		Table table = getEntityTable();

		KeyAttribute getItem = new KeyAttribute(ENTITY_KEY, entityId);
		Item item = table.getItem(getItem);
		if (item != null) {
			AttributeValue entityValue = new AttributeValue();
			entityValue.setB(ByteBuffer.wrap(entityId));
			PutItemSpec entity = new PutItemSpec();
			entity.getRequest().addExpectedEntry(ENTITY_KEY, new ExpectedAttributeValue(entityValue));
			for (String key : values.keySet()) {
				AttributeValue value = new AttributeValue();
				if (values.get(key).getClass().isAssignableFrom(Boolean.class))
					value.setBOOL((Boolean) values.get(key));
				else if (values.get(key).getClass().isAssignableFrom(String.class))
					value.setS(values.get(key).toString());
				else if (values.get(key).getClass().isAssignableFrom(Integer.class))
					value.setN(((Integer) values.get(key)).toString());
				else if (values.get(key).getClass().isAssignableFrom(Double.class))
					value.setN(((Double) values.get(key)).toString());
				else if (values.get(key).getClass().isAssignableFrom(Long.class))
					value.setN(((Long) values.get(key)).toString());
				else if (values.get(key).getClass().isAssignableFrom(Float.class))
					value.setN(((Float) values.get(key)).toString());
				// else if
				// (values.get(key).getClass().isAssignableFrom(List.class))
				// value.set(values.get(key));
				// TODO: Handle List, Map, Set. How to handle unknown types?
			}
		}
	}

	@Override
	public byte[] getRelationshipId(String relationship, String source) {
		Table table = getRelationshipTable();

		KeyAttribute getItem = new KeyAttribute(RELATIONSHIP_KEY, source + ":" + relationship);
		Item item = table.getItem(getItem);
		if (item != null) {
			return item.getBinary(CALCULATED_RELATIONSHIP_ID);
		} else {
			byte[] randomBytes = getRandomKey(table, RELATIONSHIP_KEY);

			AttributeValue nameValue = new AttributeValue();
			nameValue.setB(ByteBuffer.wrap((source + ":" + relationship).getBytes()));

			AttributeValue entityValue = new AttributeValue();
			entityValue.setB(ByteBuffer.wrap(randomBytes));

			PutItemSpec putCalculated = new PutItemSpec();
			putCalculated.getRequest().addExpectedEntry(RELATIONSHIP_KEY, new ExpectedAttributeValue(nameValue))
					.addExpectedEntry(CALCULATED_RELATIONSHIP_ID, new ExpectedAttributeValue(entityValue));
			// Create Calculated Entity Id Entry
			table.putItem(putCalculated);

			PutItemSpec entity = new PutItemSpec();
			entity.getRequest().addExpectedEntry(RELATIONSHIP_KEY, new ExpectedAttributeValue(entityValue))
					.addExpectedEntry(RELATIONSHIP_RELATIONSHIP,
							new ExpectedAttributeValue(new AttributeValue(relationship)))
					.addExpectedEntry(RELATIONSHIP_SOURCE, new ExpectedAttributeValue(new AttributeValue(source)));
			// Create Entity Entry
			table.putItem(entity);

			return randomBytes;
		}
	}

	@Override
	public Set<String> getSourceTypes() {
		Table table = getRelationshipTable();
		final Set<String> sourceTypes = new HashSet<String>();

		ScanSpec scan = new ScanSpec();
		ItemCollection<ScanOutcome> results = table.scan(scan);
		results.forEach(new Consumer<Item>() {

			@Override
			public void accept(Item t) {
				String key = new String(t.getBinary(RELATIONSHIP_KEY));
				if (key.contains(":")) {
					String[] keyParts = key.split(":");
					if (keyParts.length == 2 && keyParts[0].matches("[\\w\\s]*")) {
						sourceTypes.add(keyParts[0]);
					}
				}
			}

		});

		return sourceTypes;
	}

	@Override
	public Map<String, byte[]> getRelationshipIdsBySource(String source) {
		Table table = getRelationshipTable();
		final Map<String, byte[]> sourceTypes = new HashMap<String, byte[]>();

		ScanSpec scan = new ScanSpec().withExclusiveStartKey(new KeyAttribute(RELATIONSHIP_KEY, source));
		ItemCollection<ScanOutcome> results = table.scan(scan);
		results.forEach(new Consumer<Item>() {

			@Override
			public void accept(Item t) {
				String key = new String(t.getBinary(RELATIONSHIP_KEY));
				if (key.startsWith(source + ":")) {
					String[] keyParts = key.split(":");
					sourceTypes.put(keyParts[1], t.getBinary(CALCULATED_RELATIONSHIP_ID));
				} else {
					//TODO: Break from results call
				}
			}

		});

		return sourceTypes;
	}

	@Override
	public void establishRelationship(byte[] firstEntityId, byte[] secondEntityId, byte[] relationshipId,
			boolean isBidirectional, Map<String, Object> values) {
		Table table = getLinkTable();
		
		byte[] id = createLinkKey(firstEntityId, secondEntityId, relationshipId);
		PutItemSpec entity = createEntity(values, id);
		table.putItem(entity);
		
		if (isBidirectional) {
			byte[] secondid = createLinkKey(secondEntityId, firstEntityId, relationshipId);			
			PutItemSpec secondentity = createEntity(values, secondid);			
			table.putItem(secondentity);			
		}
	}

	private PutItemSpec createEntity(Map<String, Object> values, byte[] id) {
		AttributeValue entityValue = new AttributeValue();
		entityValue.setB(ByteBuffer.wrap(id));
		PutItemSpec entity = new PutItemSpec();
		entity.getRequest().addExpectedEntry(LINK_KEY, new ExpectedAttributeValue(entityValue));
		for (String key : values.keySet()) {
			AttributeValue value = new AttributeValue();
			if (values.get(key).getClass().isAssignableFrom(Boolean.class))
				value.setBOOL((Boolean) values.get(key));
			else if (values.get(key).getClass().isAssignableFrom(String.class))
				value.setS(values.get(key).toString());
			else if (values.get(key).getClass().isAssignableFrom(Integer.class))
				value.setN(((Integer) values.get(key)).toString());
			else if (values.get(key).getClass().isAssignableFrom(Double.class))
				value.setN(((Double) values.get(key)).toString());
			else if (values.get(key).getClass().isAssignableFrom(Long.class))
				value.setN(((Long) values.get(key)).toString());
			else if (values.get(key).getClass().isAssignableFrom(Float.class))
				value.setN(((Float) values.get(key)).toString());
			// else if
			// (values.get(key).getClass().isAssignableFrom(List.class))
			// value.set(values.get(key));
			// TODO: Handle List, Map, Set. How to handle unknown types?
		}
		return entity;
	}

	private byte[] createLinkKey(byte[] firstEntityId, byte[] secondEntityId, byte[] relationshipId) {
		ByteArrayBuilder bab = new ByteArrayBuilder();
		for (byte b : firstEntityId) {
			bab.append(b);
		}
		for (byte b : relationshipId) {
			bab.append(b);
		}
		for (byte b : secondEntityId) {
			bab.append(b);
		}
		byte[] id = bab.toByteArray();
		bab.close();
		return id;
	}
	
	private Table getLinkTable() {
		if (linkTable == null) {

			AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
			DynamoDB dynamoDB = new DynamoDB(client);
			linkTable = dynamoDB.getTable(LINK_TABLE);
		}
		return linkTable;
	}

	private Table getEntityTable() {
		if (entityTable == null) {

			AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
			DynamoDB dynamoDB = new DynamoDB(client);
			entityTable = dynamoDB.getTable(ENTITY_TABLE);
		}
		return entityTable;
	}

	private Table getRelationshipTable() {
		if (relationshipTable == null) {

			AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
			DynamoDB dynamoDB = new DynamoDB(client);
			relationshipTable = dynamoDB.getTable(RELATIONSHIP_TABLE);
		}
		return relationshipTable;
	}

	private boolean checkIfKeyIsUsed(Table table, String keyId, byte[] randomBytes) {
		KeyAttribute getItem = new KeyAttribute(ENTITY_KEY, randomBytes);
		Item item = table.getItem(getItem);
		return item != null;
	}

	private byte[] getRandomKey(Table table, String keyId) {
		byte[] randomBytes = new byte[0];
		do {
			randomBytes = RandomUtils.nextBytes(20);
		} while (!checkIfKeyIsUsed(table, keyId, randomBytes));
		return randomBytes;
	}

}
