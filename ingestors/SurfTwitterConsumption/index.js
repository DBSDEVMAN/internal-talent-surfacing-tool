var Twit = require("twit");
var T = new Twit(require("botfiles/config.js"));
var AWS = require("aws-sdk");
AWS.config.update({region: "us-west-2"});
var dynamodb = new AWS.DynamoDB({apiVersion: "2012-08-10"});
const MAX_COUNT = 200;
const IS_FOLLOWING_KEY = "IGlzIGZvbGxvd2luZyA=";
const IS_BEING_FOLLOWED_BY_KEY = "IGlzIGJlaW5nIGZvbGxvd2VkIGJ5IA==";
const IS_BEING_FOLLOWED_BY = " is being followed by ";
const IS_FOLLOWING = " is following ";

/**
 *  Grabs a list of unUsed Twitter handles in TwitterSeeds, selects one and
 *  continues with that user to get all of their twitter relationships
 */
getLastCursors = ()=>{
  var params = {
    "TableName": "TwitterSeeds",
    "ConsistentRead": true,
    "ScanFilter": {
      "IsUsed":{
        "ComparisonOperator": "EQ",
        "AttributeValueList":[
          {
            "BOOL": false
          }
        ]
      }
    },
    "Select": "ALL_ATTRIBUTES"
  };

  dynamodb.scan(params, function(err, data) {
    if (err){
      console.log(err, err.stack); // an error occurred
    } else {
      var user = data.Items.pop();
      if(!user.NextFollowingCursor) {
        user.NextFollowingCursor = { S: -1};
      }
      if(!user.NextFollowerCursor) {
        user.NextFollowerCursor = { S: -1};
      }

      T.get("users/show", {screen_name: user.TwitterHandle.S}, (err, reply)=>{
        if(err){
          return err;
        } else {
          for(i = 0; i < reply.length; i++) {
            if(reply[i].screen_name === user.TwitterHandle.S) {
              saveToEntityTable(reply[i]);
            }
          }
        }
      });
      getFollowing(user);
      getFollowers(user);
    }  
  });
};

/**
 * Takes a twitter user object and feeds it into the Entity Table
 * 
 * @param  {user}
 */
saveToEntityTable = (user)=>{

  var params = {
    "TableName": "Entity",
    "Key": {
      "entityId" : {
        "B": new Buffer(user.screen_name).toString("base64")
      }
    },
    "ReturnValues": "NONE",
    "ExpressionAttributeNames": {
      "#LK": "localKey",
      "#ID": "tw_id",
      "#SN": "tw_screenName",
    },
    "UpdateExpression": "SET #LK = :lk, #ID = :id, #SN = :sn",
    "ExpressionAttributeValues": {
      ":lk": {
        "B": new Buffer(user.id_str).toString("base64")
      },
      ":id": {
        "S": user.id_str
      },
      ":sn": {
        "S": user.screen_name
      }
    }
  };

  if(user.name) {
    params.ExpressionAttributeNames["#N"] = "tw_name";
    params.UpdateExpression = params.UpdateExpression + ", #N = :n";
    params.ExpressionAttributeValues[":n"] = {"S" : user.name};
  }
  if(user.profile_image_url_https) {
    params.ExpressionAttributeNames["#PI"] = "tw_profileImage";
    params.UpdateExpression = params.UpdateExpression + ", #PI = :pi";
    params.ExpressionAttributeValues[":pi"] = {"S" : user.profile_image_url_https};
  }
  if(user.location) {
    params.ExpressionAttributeNames["#L"] = "tw_location";
    params.UpdateExpression = params.UpdateExpression + ", #L = :l";
    params.ExpressionAttributeValues[":l"] = {"S" : user.location};
  }
  if(user.url) {
    params.ExpressionAttributeNames["#URL"] = "tw_url";
    params.UpdateExpression = params.UpdateExpression + ", #URL = :url";
    params.ExpressionAttributeValues[":url"] = {"S" : user.url};
  }
  if(user.lang) {
    params.ExpressionAttributeNames["#LANG"] = "tw_language";
    params.UpdateExpression = params.UpdateExpression + ", #LANG = :lang";
    params.ExpressionAttributeValues[":lang"] = {"S" : user.lang};
  }
  if(user.description) {
    params.ExpressionAttributeNames["#D"] = "tw_description";
    params.UpdateExpression = params.UpdateExpression + ", #D = :d";
    params.ExpressionAttributeValues[":d"] = {"S" : user.description};
  }
  if(user.time_zone) {
   params.ExpressionAttributeNames["#TZ"] = "tw_timeZone";
   params.UpdateExpression = params.UpdateExpression + ", #TZ = :tz";
   params.ExpressionAttributeValues[":tz"] = {"S" : user.time_zone}; 
  }

  postToDynamoDB(params).
  then( (res) => {
    console.log("res for saveToEntityTable", res);
  });
};

/**
 *  Saves the twitter relationship, is following or being followed by, to the 
 *  Relationship table
 * 
 * @param  {String}
 * @param  {String} or null
 * @param  {String} or null
 */
saveToRelationshipTable = (seedUserName, followerScreenName, followingScreenName)=>{

  var params = {
    "TableName": "Relationships",
    "ReturnValues": "NONE",
    "Key": { 
      "relationship": {
        //leave empty and fill key depending on follower or following
      }
    },
    "ExpressionAttributeNames": {
      "#D": "description"
    },
    "UpdateExpression": "SET #D = :d",
    "ExpressionAttributeValues": {
      ":d" : {}
    }
  };

  if(followerScreenName) {
    params.Key.relationship = {
      "B": new Buffer(seedUserName).toString("base64") + IS_BEING_FOLLOWED_BY_KEY + new Buffer(followerScreenName).toString("base64")
    };
    params.ExpressionAttributeValues[":d"] = {"S" : seedUserName + IS_BEING_FOLLOWED_BY + followerScreenName};
  }

  if(followingScreenName) {
    params.Key.relationship = {
      "B": new Buffer(seedUserName).toString("base64") + IS_FOLLOWING_KEY + new Buffer(followingScreenName).toString("base64")
    };
    params.ExpressionAttributeValues[":d"] = {"S" : seedUserName + IS_FOLLOWING + followingScreenName};
  }
 
  postToDynamoDB(params).
  then( (res) => {
    console.log("res for saveToRelationshipTable", res);
  });
};

/**
 *  Get everyone the user is following
 * 
 * @param  {Object}
 */
getFollowing = (user)=>{

  T.get("friends/list", {screen_name: user.TwitterHandle.S, count: MAX_COUNT, cursor: user.NextFollowingCursor.S}, (err, reply)=>{
    if(err){
      return err;
    } else {
      saveNextFollowingCursor(user.TwitterHandle.S, reply.next_cursor_str);
      for(i = 0; i < reply.users.length; i++) {
        saveToEntityTable(reply.users[i]);
        saveToRelationshipTable(user.TwitterHandle.S, null, reply.users[i].screen_name);
      }
    }
  });
};

/**
 * Saves the next cursor from Twitter to page through all the Twitter followers 
 * the user has
 * 
 * @param  {String}
 * @param  {String}
 */
saveNextFollowingCursor = (twitterHandle, nextCursor)=>{

  var params = {
    "TableName": "TwitterSeeds",
    "Key": {
      "TwitterHandle": {
        "S": twitterHandle
      }
    },
    "ReturnValues": "NONE",
    "ExpressionAttributeNames": {
      "#U": "IsUsed",
      "#A": "HasBeenAnalyzed",
      "#H": "IsHelpful",
      "#NF": "NextFollowingCursor"
    },
    "UpdateExpression": "SET #U = :u, #A = :a, #H = :h, #NF = :nf",
    "ExpressionAttributeValues": {
      ":u": {
        "BOOL": (nextCursor === 0 ? true : false)
      },
      ":a": {
        "BOOL": false
      },
      ":h": {
        "BOOL": false
      },
      ":nf": {
        "S": nextCursor
      }
    }
  };

  postToDynamoDB(params).
  then( (res) => {
    console.log("res for user", res);
  });
};

/**
 * Get"s everyone the user is being followed by
 * 
 * @param  {Object}
 */
getFollowers = (user)=>{
  T.get("followers/list", {screen_name: user.TwitterHandle.S, count: MAX_COUNT, cursor: user.NextFollowerCursor.S}, function (err, reply){
    if(err){
      return err;
    } else {
      saveNextFollowerCursor(user.TwitterHandle.S, reply.next_cursor_str);
      for(i = 0; i < reply.users.length; i++) {
        saveToEntityTable(reply.users[i]);
        saveToRelationshipTable(user.TwitterHandle.S, reply.users[i].screen_name, null);
      }
    }
  });
};

/**
 * Saves the twitter page cursor, in order to continue moving through the seed"s 
 * followers
 * 
 * @param  {String}
 * @param  {String}
 */
saveNextFollowerCursor = (twitterHandle, nextCursor)=>{

  var params = {
    "TableName": "TwitterSeeds",
    "Key": {
      "TwitterHandle": {
        "S": twitterHandle
      }
    },
    "ReturnValues": "NONE",
    "ExpressionAttributeNames": {
      "#U": "IsUsed",
      "#A": "HasBeenAnalyzed",
      "#H": "IsHelpful",
      "#NFR": "NextFollowerCursor"
    },
    "UpdateExpression": "SET #U = :u, #A = :a, #H = :h, #NFR = :nfr",
    "ExpressionAttributeValues": {
      ":u": {
        "BOOL": (nextCursor === 0 ? true : false)
      },
      ":a": {
        "BOOL": false
      },
      ":h": {
        "BOOL": false
      },
      ":nfr": {
        "S": nextCursor
      }
    }
  };

  postToDynamoDB(params).
  then( (res) => {
    console.log("res for user", res);
  });
};

/**
 * converted callback to Promise
 *
 * @param      {Object}   params  The parameters
 * @return     {Promise}  { description_of_the_return_value }
 */
postToDynamoDB = (params)=>{
  return new Promise((resolve, reject) => {
    dynamodb.updateItem(params, (err, data) => {
      if (err){ 
        console.log("These params were rejected: ", params);
        console.log("err: ", err);
        reject(err);
      } else {
        resolve(data);
      }                
    });
  });
};

exports.handler = function myBot(event, context) {
  //we can only pull 15 times every 15 minutes
  for(i = 0; i < 15; i++) {
    //delaying for 3 seconds to allow for the next cursor to be saved
    setTimeout(getLastCursors, 3000);
  }
};