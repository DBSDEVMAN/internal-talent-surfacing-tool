# -*- coding: utf-8 -*-
"""
Contains a series of helper function for Meetup
Ingestion.
Author: Luke Garzia
Date: 6/1/2017
import Meetup_Ingester_Utility as mu_ut
"""
from boto3.dynamodb.conditions import Key, Attr
import SURF_Ingester_Utility as surf
from pandas import DataFrame
import requests
import time
import logging
import json

__apikey = '1858355c3f25435b3bd321f677b3544'

#6/1/2017: LG - ideas - enhance to delete, rebuild table, or use put item going forward. 
# Depending on use cases - review in batch or not
def loadInterestingMeetups() -> 'Uses Batch Write: Will not update table':
	""" Note: The logic here uses Batch Write - to rebuild - delete table."""
	print('hello world')
	group_list = [
		{'group_id' :1566558, 'name': 'St. Louis Java Users Group', 'active':1} ,
		{'group_id' :4325882, 'name': 'St. Louis Machine Learning & Data Science', 'active':1},
		{'group_id' :1772780, 'name': 'Saint Louis R User Group', 'active':1},
		{'group_id' :20979516, 'name': 'St. Louis Cloudera User Group', 'active':1},
		{'group_id' :6240052, 'name': 'STL Python', 'active':1},
		{'group_id' :1706946, 'name': 'St. Louis Hadoop Users Group', 'active':1},
		{'group_id' :16533162, 'name': 'St. Louis Azure User Group', 'active':1},
		{'group_id' :18357785, 'name': 'STL Angular Meetup', 'active':1},
		{'group_id' :13928352, 'name': 'Saint Louis Association of IT Professionals', 'active':1},
		{'group_id' :19806092, 'name': 'St. Louis .NET User Group', 'active':1},
		{'group_id' :22578565, 'name': 'CWE Coding Hangouts', 'active':1},
		{'group_id' :11366242, 'name': 'Docker St. Louis', 'active':1},
		{'group_id' :1662255, 'name': 'St. Louis Game Developers', 'active':1},
		{'group_id' :21089517, 'name': 'Saint Louis Deep Learning Meetup', 'active':1},
		{'group_id' :8985132, 'name': 'Open Data STL', 'active':1},
		{'group_id' :13206522, 'name': 'Papers We Love (Saint Louis Chapter)', 'active':1},
		{'group_id' :18509939, 'name': 'Ansible St. Louis', 'active':1},
		{'group_id' :3842532, 'name': 'Interactive Developers of St. Louis', 'active':1},
		{'group_id' :19052755, 'name': 'GDG St Louis: Google Developers Group St Louis', 'active':1},
		{'group_id' :2881962, 'name': 'Saint Louis MongoDB User Group', 'active':1},
		{'group_id' :19712645, 'name': 'St. Louis IoT Meetup', 'active':1},
		{'group_id' :18515021, 'name': 'DAMA STL', 'active':1},
		{'group_id' :9505322, 'name': 'Big Data Developers in St. Louis', 'active':1},
		{'group_id' :2536392, 'name': 'St. Louis Metro East .NET User Group', 'active':1},
		]

#http://boto3.readthedocs.io/en/latest/reference/services/dynamodb.html#DynamoDB.Table.batch_writer
	t =  surf.getDynamoServiceTable('meetupsOfInterest')    
	print("Loading")
	with t.batch_writer() as batch: #If want sdk to handle dedupe overwrite_by_pkeys=['group_id']
		for item in group_list:
			batch.put_item(item)
	print("done")

def retrieveMeetupsOfInterest() -> 'Returns a Pandas DataFrame':
	table = surf.getDynamoServiceTable('meetupsOfInterest')
	response = table.scan(FilterExpression=Attr('active').eq(1))
	return DataFrame(response['Items'])

def loadMeetupRelationships(fCnt:'itertools function counter') -> 'Uses Batch Write: Will not update table':
	"""One Time Load in DynamoDB"""
	rel = ('is member of', 'has membership from', 'is interested in', 'has interest from')
	#Generate a sequence of items 
	
	def createItem(description:str, fCnt:'Counter Function'):
		id = surf.getNextKey('meetup', fCnt)
		if id is not None:
			return {'relationshipType':id, 'name':description, 'datasource':'meetup'}
	
	surf.batchWriteListOfDictionary('RelationshipType',[createItem(x, fCnt) for x in rel])
	
def retrieveMemberInformation(memberID:'local meetup membership id') -> 'Return None of Dictionary of data':
		url = 'https://api.meetup.com/members/'+ str(memberID)+'?photo-host=public&page=200&fields=memberships%2Cmessaging_pref%2C+other_services%2C+privacy%2C+stats%2C+topics'+'&signed=true&key='+str(__apikey)
		return surf.executeRESTFulAPI(url)
		
def retrieveGroupInformation(groupUrl:'Url For Group') -> 'Return None of Dictionary of data':
		url = 'https://api.meetup.com/'+ str(groupUrl)+'?photo-host=public&fields=topics%2C+approved%2C+event_sample%2C+last_event%2C+leads%2C+list_mode%2C+plain_text_description'+'&signed=true&key='+str(__apikey)	
		return surf.executeRESTFulAPI(url)

def	retrieveTopicInformation(topicUrl:'UrlName for Topic')->'Return None or Dictionary for data':
		url = 'https://api.meetup.com/topics?offset=0&format=json&topic='+ str(topicUrl)+'&photo-host=public&page=20&order=members&signed=true&key='+str(__apikey)	
		return surf.executeRESTFulAPI(url)
		
def	retrieveMembersInGroupsInformation(groupId:'Id for Group Number', attempts=0)->'Return None or Dictionary for data':
		url = 'https://api.meetup.com/2/profiles?&sign=true&photo-host=public&group_id='+str(groupId)+'&page=200&offset=0&page=200&signed=true&key='+str(__apikey)
		
		member_list = []
		try:		
			while(1==1):
				response =  surf.executeRESTFulAPI(url)
				(result, calls_left, time_remaining) = surf.processResponse(response, True)
				surf.isItSafeToCallAPI(calls_left, time_remaining)
				if len(result['results']) > 0: 
					member_list += [x['member_id'] for x in result['results']]
				url = result['meta']['next']
				if url == '': 
					break
			return member_list
		except: 
			logging.debug('Waiting 60 seconds because of API Flakiness group id {}'.format(groupId))
			logging.debug(response.text)
			time.sleep(60)
			if attempts <= 10:
				attempts +=1
				return retrieveMembersInGroupsInformation(groupId, attempts)
			else:
				raise e
		
		
				
def processEntity(entityType:str, keyTuple:'(globalId, dynlocalId, url, localId)', rel = None, Entity1Key = None, Entity1Item  = None, attempts=0):
	if not surf.fCheck(keyTuple[1]): #Need to rethink... if process fails...doesn't retry again
                
		if entityType=='MEMBER':
			response = retrieveMemberInformation(keyTuple[3]) #Uses source local id 
		elif entityType=='TOPIC':
			response = retrieveTopicInformation(keyTuple[2])
		elif entityType == 'GROUP':
			response = retrieveGroupInformation(keyTuple[2])
		else:
			raise Exception('Invalid Entity Type')
		try:
			payload, xlimit, xreset = surf.processResponse(response, need_meetup_header = True)
		except requests.exceptions.HTTPError as e:
			if e.response.status_code == 410: #Indicates group no longer available
				logging.debug('group no longer exists return empty list')
				return [], None
			elif e.response.status_code == 504:	
				logging.debug('Waiting 60 seconds because there was a gateway timeout')
				time.sleep(60)
				if attempts <= 10:
					attempts +=1
					return processEntity(entityType, keyTuple, rel, Entity1Key, Entity1Item, attempts)
				else:
					raise e
			else:
				raise
		except json.decoder.JSONDecodeError: #Flaky API - wait 30 seconds and try again
			logging.debug('Waiting 60 seconds because of API Flakiness - JSONDecodeError')
			time.sleep(60)
			if attempts <= 10:
				attempts +=1
				return processEntity(entityType, keyTuple, rel, Entity1Key, Entity1Item, attempts)
			else:
				raise e
		except: #catch the rest
			raise

#			payload, xlimit, xreset = surf.processResponse(response, need_meetup_header = True)
		if rel == 'Topics':	
			entityItem = surf.generateItemsDictFromJson(payload['results'][0],'Entity', keyTuple) #this adjust the response
		else:
			try:
				entityItem = surf.generateItemsDictFromJson(payload,'Entity', keyTuple) #this adjust the response
			except UnboundLocalError as e:
				logging.debug('print payload empty? Why? for {}'.format(keyTuple))
				raise e
		surf.isItSafeToCallAPI(xlimit, xreset)
		surf.batchWriteListOfDictionary('Entity', [entityItem])
		#Need to adjust hardcoding lookup 'mu_name'
		if rel is not None:
			rItems = buildRelationship(Entity1Key, keyTuple[0],  Entity1Item['mu_name'], entityItem['mu_name'], rel)
			surf.batchWriteListOfDictionary('Relationships', rItems)
		return entityItem, payload
	else: 
		return ([],[])
def buildRelationship(entity1:'Bytes', entity2:'Bytes', entity1Name, entity2Name, relationshipDescription:str):
	
	#print(surf.RELATIONSHIP_TYPES[relationshipDescription][0])
	
	relationship = entity1.value.decode(encoding = 'utf-8') + surf.RTYPE[surf.RELATIONSHIP_TYPES[relationshipDescription][0]].value.decode(encoding='utf-8') + entity2.value.decode(encoding = 'utf-8')               
	description = entity1Name + ' ' + surf.RELATIONSHIP_TYPES[relationshipDescription][0] + ' ' + entity2Name
	rItem = [{'relationship':bytes(relationship, encoding='utf-8'), 'description': description}]
	#Do the inverse of the relationship
	relationship2 = entity2.value.decode(encoding = 'utf-8') + surf.RTYPE[surf.RELATIONSHIP_TYPES[relationshipDescription][1]].value.decode(encoding='utf-8') + entity1.value.decode(encoding = 'utf-8')               
	description2 = entity2Name + ' ' + surf.RELATIONSHIP_TYPES[relationshipDescription][1] + ' ' + entity1Name			
	rItem.append({'relationship':bytes(relationship2, encoding='utf-8'), 'description': description2})
	
	return rItem

def logtest():
	logging.debug('test me')