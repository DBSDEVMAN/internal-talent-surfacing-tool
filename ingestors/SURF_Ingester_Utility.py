# -*- coding: utf-8 -*-
"""
Contains a series of helper function for Surf
Project.
Author: Luke Garzia
Date: 6/1/2017
import SURF_Ingester_Utility as surf
"""
import boto3
import pickle
import itertools
import logging
from boto3.dynamodb.conditions import Key, Attr
from collections import namedtuple
import requests
import decimal
from boto3.dynamodb.types import Binary
import botocore.exceptions
import sys

#Create a Global Constant that defines the named datasources
DS = namedtuple('DS', "meetup github")
ENTITY_TYPES = {'MEMBER':'MEMBER', 'TOPIC':'TOPIC', 'GROUP':'GROUP'}
DATA_SOURCES = DS(meetup = "meetup",github="github") 
DATA_KEYS = {'Entity':('entityId','localKey'), 'dev_Entity':('entityId','localKey')}
DS_PREFIX = {'meetup':'mu', 'github':'gu'}
RELATIONSHIP_TYPES = {'Membership':('is member of', 'has membership from'), 
                     'Topics':('is interested in', 'has interest from'),
                     'Organizer':('is organizer of', 'is organized by')}


from pandas import DataFrame


AWS_SECRET_KEY = 'H+/d5PwUovPCNX1hyRhbreNcvU0xJuK5v+FjbqaG'
REGION_NAME = 'us-west-2'
AWS_ACCESS_KEY_ID ='AKIAITYIYEQBQ2QAJ2PQ' 
def getDynamoServiceClient() -> 'DynamoDB Client Service':
	""" See Function: Run Get all Meetups in Saint Louis:
	Function will return an Excel File of all meetups 50 miles
	from Daugherty Headquarter"""
	return boto3.client('dynamodb', region_name=REGION_NAME, aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_KEY)

def getDynamoServiceTable(tableName:str) -> 'DynamoDB Table Service':
	""" Table is a main, high level resource from DynamoDB"""
	dynamodb = boto3.resource('dynamodb', region_name=REGION_NAME, aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_KEY)
	return dynamodb.Table(tableName)

def getCounterFunction() -> 'returns an itertools.count function':
	"""Function will get counter value from DynamoDB - store value to seed Counter and store in memory"""
	#Step 1 is read counter from pickle
	currentValue = pickle.load(open( "counter.pkl", "rb" ))
	return itertools.count(currentValue+1)

def getNextKey(datasource:'Name Datasource') -> 'returns 20 Byte Binary Data':
	"""Expect defined datasource [meetup, github] """
	def getKey(i):
			#logging.debug('in getKey function')
			id = next(_cFunc) #capture next value
			pickle.dump(id, open( "counter.pkl", "wb" ))#write latest value to avoid duplicates
			return Binary(bytes(str(i), encoding='utf-8') + bytes.zfill(bytes(str(id), encoding='utf-8'),19))

	if datasource == 'meetup':
		return getKey(1)
	elif datasource=='github':
		return getKey(2)
	else:
		raise Exception('Invalid Datasource')
	#Need to build up for rest of datasources

def returnKey(tableName:'str', localId:'local datasource key', datasource:'Name Datasource', prefix='', url='')->'return tuple key':
	"""Function locates existins key, returns that value, otherwise generates a new key"""
	#Part 1 - check for existing table
	localKey = prefix + str(localId)
	table = getDynamoServiceTable(tableName) #wrap this in a try
	local = _generateLocalKey(datasource, localKey)
	response = table.get_item(Key={DATA_KEYS[tableName][0]: local})
	try:
		return (response['Item']['localKey'], local, url, localId, 'EXISTING')
	except:
		#assume new local id - generate a global key
		d_key = getNextKey(datasource)
		_loadLocalKey(tableName, local, d_key, datasource) #wrap in try
		return (d_key, local, url, localId, 'NEW')
def _loadLocalKey(tableName:'str', localKey:'20 Byte Character', dynamoKey:'20 Byte Character', datasource:'named datasource'):
	""" Function loads local key into """ 
	#Step 1 is get local Table resouce
	table = getDynamoServiceTable(tableName)
	#Wrap this in a Try/Execption Logic
	table.put_item(
		Item={
        DATA_KEYS[tableName][0]: localKey,
        DATA_KEYS[tableName][1]: dynamoKey})
		
def _generateLocalKey(datasource:'str', localKey:'Local Key')-> 'Returns bytes encoded string':
	"""Function concatenates the datasource prefex with localkey"""
	def concateSourceKey(source, key):
		return bytes(source + '_'+str(key), encoding = 'utf-8')
	if datasource == 'meetup':
		return concateSourceKey("mu", localKey)
	elif datasource=='github':
		return getKey("gu", localKey)
	else:
		raise Exception('Invalid Datasource')
	
def batchWriteListOfDictionary(tableName:str, itemList:'List of Item Dictionary') -> 'Return None':
	"""Batch Write is a simple looping mechanism. It'll opened a service connection to tablename
	and loop through list"""
	#http://boto3.readthedocs.io/en/latest/reference/services/dynamodb.html#DynamoDB.Table.batch_writer
	t =  getDynamoServiceTable(tableName)    
	#logging.debug("Loading {}".format(tableName))
	try:
		with t.batch_writer() as batch: #If want sdk to handle dedupe overwrite_by_pkeys=['group_id']
			for item in itemList:
				batch.put_item(item)
	#logging.debug("Done loading {}".format(tableName))
	except botocore.exceptions as e:
		debug.logging('Error in batch Write')
		if 'Item size has exceeded the maximum allowed' in str(e):
			debug.logging('Item id is {} the name is {} and size is {}'.format(item['mu_id'], item['mu_name'], sys.getsizeof(item)))
			raise
def putWriteItemDictionary(d_Item:'Item Dictionary', tableName:'') ->'':
	t =  getDynamoServiceTable(tableName)  
	t.put_item(Item = d_Item)
def retrieveRelationShipType(datasource:'str')-> 'Returns Pandas DataFrame':
	""" Expected values for datasource is meetup,..."""
	table = getDynamoServiceTable('RelationshipType')
	response = table.scan(FilterExpression=Attr('datasource').eq(datasource))
	rtype = DataFrame(response['Items'])
	return {rtype.iloc[n]['name']: rtype.iloc[n]['relationshipType'] for n in range(len(rtype.index))}

def isItSafeToCallAPI(calls_remaining, time2wait):
    '''Will pause program if needed'''
    if int(calls_remaining) <= 1:
        logging.debug('Going to sleep for {} seconds'.format(result.headers['X-RateLimit-Reset']))
        sleep(int(time2wait))
    return True
	
def loadEntityFromApiToDynamo(datasource:'str', entityKey:'20 Bytes Datatype') ->'Return True/False':
	""" Generic function that loads API call - idea is to pass along the returned object that's 
	been formated per requirements"""
	pass

def processResponse(response, need_meetup_header = False):
	""" If need meetup header for time remaining - pass in a True """
	if response.status_code == 200:
		if need_meetup_header:
			return response.json(), response.headers['X-RateLimit-Remaining'], response.headers['X-RateLimit-Reset']
		else:
			return response.json()
	else:
		response.raise_for_status()


def executeRESTFulAPI(url:str) -> 'Returns JSON Dictionary, Time Remaining, Time to Wait':
	try:
		response = requests.get(url)	
	except:
		raise #Reraise the network issue
	else:
		return response
def _recurseListConvertFloat2Decimal(listItem):
	"""Recursivly update dictionary"""
	for item in listItem:
		if isinstance(item, float):
			item = decimal.Decimal(str(item))
		elif isinstance(item, str):
			if item == '':
				item = '_'
		elif isinstance(item, dict):
			
			_recurseDictConvertFloat2Decimal(item)
		elif isinstance(item, list):
			_recurseListConvertFloat2Decimal(item)
	return listItem	

def _recurseDictConvertFloat2Decimal(dictItem):
	"""Recursivly update dictionary"""
	for key, value in dictItem.items():
		if isinstance(value, float):
			dictItem[key] = decimal.Decimal(str(value))
		elif isinstance(value, str):
			if value == '':
				dictItem[key] = '_'
		elif isinstance(value, dict):
			_recurseDictConvertFloat2Decimal(value)
		elif isinstance(value, list):
			_recurseListConvertFloat2Decimal(value)
	return dictItem	
	
def generateItemsDictFromJson(dSJON:'dictionary', tableName:str, keyTuple:'tuple') ->'Return ITEMS Dictionary':
	"""Logic will update the key names to include prefix"""
	Items = dict(zip(map(lambda x: 'mu_'+x, dSJON.keys()), dSJON.values()))
	Items[DATA_KEYS[tableName][0]] = keyTuple[0]
	Items[DATA_KEYS[tableName][1]] = keyTuple[1]
	Items = _recurseDictConvertFloat2Decimal(Items) #Not pure functional; mutatating dictionary object
	return Items

def testIfCallMadeDuringProcess():
	mem = set()
	def testIfCallInSet(item:str):
		"""Uses a simple set closure to test if call is already made - avoid Meetup Call"""
		if item in mem:
			return True
		else:
			mem.add(item)
			return False
	return testIfCallInSet
		
def testLogger():
	logging.debug('hello')

RTYPE = retrieveRelationShipType("meetup")
_cFunc = getCounterFunction() 
fCheck = testIfCallMadeDuringProcess()