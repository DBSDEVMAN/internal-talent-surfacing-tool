var Twit = require("twit");
var T = new Twit(require("botfiles/config.js"));
var AWS = require("aws-sdk");
AWS.config.update({region: "us-west-2"});
var dynamodb = new AWS.DynamoDB({apiVersion: "2012-08-10"});
//count Twitter MaxCount
const MAX_COUNT = 200;

  /**
   * Gets the Twitter followers from the twitterHandle and starts at the cursor location for the collection.
   *
   * @param      {String}  twitterHandle   The twitter handle
   * @param      {String}  cursorLocation  The cursor location
   */
   getFollowers = (twitterHandle, cursorLocation) => {

    T.get("followers/list", {screen_name: twitterHandle, count: MAX_COUNT, cursor: cursorLocation}, (err, reply) => {
      if(err){
        return err;
      } else {
        saveNextCursor(twitterHandle, reply.next_cursor_str);
        postUsersToDynamo(reply.users);
      }
    });
  };

  /**
   * Creates post parameters for DynamoDB.
   *
   * @param      {object}  entity  The entity
   */
  createPostParams = (entity) => {
    
    var params = {
      "TableName": "TwitterSeeds",
      "Key": {
        "TwitterHandle": {
          "S": entity.screen_name
        }
      },
      "ReturnValues": "NONE",
      "ExpressionAttributeNames": {
        "#U": "IsUsed",
        "#A": "HasBeenAnalyzed",
        "#H": "IsHelpful"
      },
      "UpdateExpression": "SET #U = :u, #A = :a, #H = :h",
      "ExpressionAttributeValues": {
        ":u": {
          "BOOL": false
        },
        ":a": {
          "BOOL": false
        },
        ":h": {
          "BOOL": false
        }
      }
    };

    if(entity.name) {
      params.ExpressionAttributeNames["#N"] = "Name";
      params.UpdateExpression = params.UpdateExpression + ", #N = :n";
      params.ExpressionAttributeValues[":n"] = {"S": entity.name}; 
    }
    if(entity.location) {
      params.ExpressionAttributeNames["#L"] = "Location";
      params.UpdateExpression = params.UpdateExpression + ", #L = :l";
      params.ExpressionAttributeValues[":l"] = {"S": entity.location}; 
    }
    if(entity.description) {
      params.ExpressionAttributeNames["#D"] = "Description";
      params.UpdateExpression = params.UpdateExpression + ", #D = :d";
      params.ExpressionAttributeValues[":d"] = {"S": entity.description}; 
    }
    if(entity.nextCursor) {
      params.ExpressionAttributeNames["#NC"] = "nextCursor";
      params.UpdateExpression = params.UpdateExpression + ", #NC = :nc";
      params.ExpressionAttributeValues[":nc"] = {"S": entity.nextCursor};  
    }

    return params;
  };

  /**
   * Posts all users from Twitter to DynamoDB.
   *
   * @param      {Array[Object]}  users   The users
   */
  postUsersToDynamo = (users)=>{

     users.forEach((follower)=>{
      postToDynamoDB(createPostParams(follower)).
        then( (res) => {
          console.log("res for user: ", res);
        });
     });
  };

  /**
   * converted callback to Promise
   *
   * @param      {Object}   params  The parameters
   * @return     {Promise}  { description_of_the_return_value }
   */
  postToDynamoDB = (params)=>{
    return new Promise((resolve, reject) => {
      dynamodb.updateItem(params, (err, data) => {
        if (err){ 
          console.log("These params were rejected: ", params);
          console.log("err: ", err);
          reject(err);
        } else {
          resolve(data);
        }                
      });
    });
  };

  /**
   * Saves the location of the next cursor in order to save it DynamoDB for the Lambda 
   * function to continue in the Twitter collections from where it left off.
   *
   * @param      {String}  twitterHandle  The twitter handle
   * @param      {String}  nextCursor     The next cursor
   */
  saveNextCursor = (twitterHandle, nextCursor)=>{
    postToDynamoDB(createPostParams({
      screen_name: twitterHandle,
      nextCursor: nextCursor
    })).
      then((res) => {
        console.log("res for NextCursor", res);  
      });
  };

  /**
   * Gets the next cursor in order to continue in the same spot in the collection, the next time the Lambda function wakes up.
   *
   * @param      {String}  twitterHandle  The twitter handle
   */
  function getNextCursor(twitterHandle){

    var params = {
      "Key": {
        "TwitterHandle": {
          "S": twitterHandle
        }
      },
      "TableName": "TwitterSeeds",
      "ConsistentRead": true,
      "ProjectionExpression": "nextCursor"
    };

    dynamodb.getItem(params).
      on("success", (res) => {

        var nextCursor = parseInt(res.data.Item.nextCursor.S);

        if(nextCursor > 0){
          getFollowers(twitterHandle, nextCursor);
        } else if (nextCursor === 0) {
          //since data.next_cursor is === 0, we know that we"ve gone through all the followers
          console.log("Fin. All seeds have been pulled from the supplied Twitter handle. This Lambda function can be turned off.");
        } else {
          //there was no previous cursor saved, so we need to start on page one, next_cursor === -1
          getFollowers(twitterHandle, -1);
        }
      }).
      on("error", (res)=> console.log("res", res)).
      send();
  }

 exports.handler = function myBot(event, context) {
  //we can only pull from this twitter api 15 times every 15 minutes
  for(i = 0; i < 15; i++) {  
    //setting a 3 second pause between loops in order for the nextCursor to be  
    //saved to the db before attempting to pull the next page 
    setTimeout(getNextCursor, 3000, "daughertytweets"); 
  } 
 };