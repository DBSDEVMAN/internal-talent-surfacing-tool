# -*- coding: utf-8 -*-
"""
Created on Tue May 30 09:36:14 2017

@author: lgarzia
"""
#Meetup 2
#Standard Library Imports
import os,logging,argparse,imp,SURF_Ingester_Utility as surf,Meetup_Ingester_Utility as mu_ut
#os.chdir(r'''C:\Users\lgarzia\Documents\Daugherty\SURF\Ingestions''')
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
parser = argparse.ArgumentParser()
parser.add_argument(
    '--debug_level', choices=('CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'),
    action="store", dest='debug_level', default='DEBUG')
parser.add_argument('--refresh', action="store_true", default=False, help='Include if want refresh existing data with calls to MU API')

args = parser.parse_args() #Capture the argparser variables
##Dictionary
loglevel = {'CRITICAL':50,'ERROR':	40, 'WARNING':30, 'INFO':20, 'DEBUG':10, 'NOTSET':0}
logger = logging.getLogger()


#imp.reload(mu_ut)
#imp.reload(surf)


def main(REFRESH):
    '''Main Loop to process generator'''
    #REFRESH = False
    logging.info('User chose to refresh data: {}'.format(REFRESH))
    #Step 1: Get the meetups of interest defined by HR
    logging.debug('Retrieving the HR groups of interest')
    df_mu = mu_ut.retrieveMeetupsOfInterest()
    GROUPS_LEFT = len(df_mu)
    logging.debug('There are {} groups to process'.format(GROUPS_LEFT))
    for index, row in df_mu.iterrows():   
        logging.info('Working On {}: there are {} groups left'.format(row['name'], GROUPS_LEFT))
        GROUPS_LEFT -= 1
        #Part 1: loop through each group
        memberDict = mu_ut.retrieveMembersInGroupsInformation(int(row['group_id']))
        MEMBERS_LEFT = len(memberDict)
        logging.info('There are {} members in {}'.format(MEMBERS_LEFT, row['name']))
        #QA here [']
        for memberId in memberDict: #subset:
            logging.debug('There are {} members left'.format(MEMBERS_LEFT))
            MEMBERS_LEFT -= 1
            #retrieve the global key and local key (#should be a namedTuple...)
            memberKeyTuple = surf.returnKey('Entity', memberId, surf.DATA_SOURCES.meetup)
            #memberKeyTuple[4] - NEW/EXISTING -> if EXISTING & REFRESH == TRUE go get 
            if memberKeyTuple[4] =='NEW' or REFRESH == True:
                memberItem, mpayload = mu_ut.processEntity(surf.ENTITY_TYPES['MEMBER'], memberKeyTuple)
            else: 
                memberItem =[] #Setting Member Item to empty list
            if memberItem == []:
                logger.debug('Skipping Member ID {}'.format(memberKeyTuple))
            else:
                logger.debug('Just pulled information for {}'.format(memberItem['mu_name']))
                logger.debug('Starting to load topics for {}'.format(memberItem['mu_name']))
                try:
                    topicItemsCleaned = [surf.generateItemsDictFromJson(*x) for x in 
                                     [(x, 'Entity', surf.returnKey('Entity', x['id'], surf.DATA_SOURCES.meetup, 'tpc_', x['urlkey'] )) for x in  mpayload['topics']]]
                    surf.batchWriteListOfDictionary('Entity',topicItemsCleaned)
                    logger.debug('Starting to load relationships to topics for {}'.format(memberItem['mu_name']))
                    relationshipsTopic = [mu_ut.buildRelationship(memberKeyTuple[0], x['entityId'], memberItem['mu_name'], x['mu_name'], 'Topics') for x in topicItemsCleaned]        
                    [surf.batchWriteListOfDictionary('Relationships', x) for x in relationshipsTopic]
                    logger.debug('Just completed loading topic information')
                except KeyError:
                    logger.debug('User has no topics')
                #Cycle through all groups member belongs too; each group cycle through 
                try:
                    memberGroupIds = [(x['group']['urlname'], x['group']['id']) for x in mpayload['memberships']['member']] #Returns all groups member of
    
                    for groupUrl, groupId in memberGroupIds:
                        logging.debug('Starting to load collect group information for {}'.format(memberItem['mu_name']))                    
                        groupKeyTuple = surf.returnKey('Entity', groupId, surf.DATA_SOURCES.meetup, 'grp_', groupUrl)#move to SURF
                        if groupKeyTuple[4] =='NEW' or REFRESH == True:
                            groupItem, gpayload = mu_ut.processEntity(surf.ENTITY_TYPES['GROUP'], groupKeyTuple, rel='Membership', Entity1Key = memberKeyTuple[0], Entity1Item=memberItem)#process entity prefix
                        else:
                            groupItem = []
                        if groupItem != []: #Already processed    
                            logging.debug('Just loaded membership information on group {}'.format(groupItem['mu_name']))
                            #----------------------------------------------------------------------------------------------------------------------#
                         #   groupTopicIds = [(x['urlkey'], x['id']) for x in gpayload['topics']]
                            try:     
                                logging.debug('Starting to collect Topic information on group {}'.format(groupItem['mu_name']))
                                gtopicItemsCleaned = [surf.generateItemsDictFromJson(*x) for x in 
                                                 [(x, 'Entity', surf.returnKey('Entity', x['id'], surf.DATA_SOURCES.meetup, 'tpc_', x['urlkey'] )) for x in gpayload['topics']]]
                                surf.batchWriteListOfDictionary('Entity', gtopicItemsCleaned)
                                logging.debug('Starting to load Group Topic Relationships for {}'.format(groupItem['mu_name']))
                                grelationshipsTopic = [mu_ut.buildRelationship(groupKeyTuple[0], x['entityId'], groupItem['mu_name'], x['mu_name'], 'Topics') for x in gtopicItemsCleaned]        
                                [surf.batchWriteListOfDictionary('Relationships', x) for x in grelationshipsTopic]
                                logger.debug('Just completed loading topic information for {}'.format(groupItem['mu_name']))
                            except KeyError:
                                logger.debug('Group has no topics')
                        else:
                             logging.debug('Group already processed')
                    #    #Build the Relationship                  
                    organizerGroupsIds =  [(x['group']['urlname'], x['group']['id']) for x in mpayload['memberships']['organizer']]           
                    for groupUrl, groupId in organizerGroupsIds:
                        groupKeyTuple = surf.returnKey('Entity', groupId, surf.DATA_SOURCES.meetup, 'grp_', groupUrl)
                        if groupKeyTuple[4] =='NEW' or REFRESH == True:
                            GroupItem, gpayload = mu_ut.processEntity(surf.ENTITY_TYPES['GROUP'], groupKeyTuple,  rel='Organizer',Entity1Key= memberKeyTuple[0],Entity1Item=memberItem )    
                            logging.debug('Just loaded organization information Orgnanized groups')
                        else:
                             logging.debug('Group already processed')
                    logger.debug('Just completed {} in group {}'.format(memberItem['mu_name'], row['name']))
                except KeyError:
                    logger.debug('User has no membership - see privacy setting')

if __name__ ==  "__main__":
    print('hello world')
    logger.setLevel(loglevel[args.debug_level])    
    main(args.refresh)
      


