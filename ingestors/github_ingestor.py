# -*- coding: utf-8 -*-
"""
Created on Fri May 19 14:43:06 2017

@author: rtdew
"""

import requests
import json
import boto3
import binascii
import datetime

tablePrefix = "dev_"
# Do not hard code credentials
dynamodb = boto3.resource('dynamodb', region_name='us-west-2', aws_access_key_id='AKIAITYIYEQBQ2QAJ2PQ', aws_secret_access_key='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')
entity = dynamodb.Table(tablePrefix + 'Entity')
restURL = 'https://api.github.com'
restToken = '5021d1d0f22d648c4a4ec58ed584a2ac0721b8e1'

def getNextApiPage(apiResponse):
    print(apiResponse.headers)
    
def putEntity(githubUser):
    
    resp = requests.get(restURL + '/users/' + githubUser + '?access_token=' + restToken)
    
    if resp.status_code != 200:
        # This means something went wrong.
        #raise ApiError('GET /me/ {}'.format(resp.status_code))
        print(resp.text)
        
    else:
        #print(resp.text)
        jsonResp = json.loads(resp.content)
        githubLogin = jsonResp["login"]
        githubId = jsonResp["id"]
        githubAvatarURL = jsonResp["avatar_url"]
        githubName = jsonResp["name"]
        githubCompany = jsonResp["company"]
        githubBlog = jsonResp["blog"]
        githubLocation = jsonResp["location"]
        githubEmail = jsonResp["email"]
        githubHireable = jsonResp["hireable"]
        githubBio = jsonResp["bio"]
        githubPublicRepos = jsonResp["public_repos"]
        githubPublicGists = jsonResp["public_gists"]
        githubFollowerCount = jsonResp["followers"]
        githubFollowingCount = jsonResp["following"]
        githubAccountCreatedAt = jsonResp["created_at"]
        githubAccountUpdatedAt = jsonResp["updated_at"]
        
        print("Putting githubLogin")
        
        entityIdStr = "githubLogin_" + str(githubId)
        binaryId = binascii.a2b_qp(entityIdStr)
        
        #binaryId = Binary(entityIdStr.to_bytes(1, byteorder='big', signed=True))
        #binaryId = ' '.join(format(x, 'b') for x in bytearray(entityIdStr))
        #binaryId = bin(int.from_bytes(entityIdStr.encode(), 'big'))
        #binaryId = bin(int(binascii.hexlify(entityIdStr), 16))
        
        item = {
                'entityId': binaryId
            }
        # Populate the values in the Item only if values are present in the data returned
        if githubLogin:
                item['gh_login'] = githubLogin
        if githubId:
                item['gh_id'] = githubId
        if githubAvatarURL:
                item['gh_avatar_url'] = githubAvatarURL
        if githubName:
                item['gh_name'] = githubName
        if githubCompany:
                item['gh_company'] = githubCompany
        if githubBlog:
                item['gh_blog'] = githubBlog
        if githubLocation:
                item['gh_location'] = githubLocation
        if githubEmail:
                item['gh_email'] = githubEmail
        if githubHireable:
                item['gh_hireable'] = githubHireable
        if githubBio:
                item['gh_bio'] = githubBio
        if githubPublicRepos:
                item['gh_public_repos'] = githubPublicRepos
        if githubPublicGists:
                item['gh_public_gists'] = githubPublicGists
        if githubFollowerCount:
                item['gh_follower_count'] = githubFollowerCount
        if githubFollowingCount:
                item['gh_following_count'] = githubFollowingCount
        if githubAccountCreatedAt:
                item['gh_account_created_at'] = githubAccountCreatedAt
        if githubAccountUpdatedAt:
                item['gh_account_updated_at'] = githubAccountUpdatedAt
        
        
        try:
            entity.put_item(
                Item=item
            )
        except Exception:
            print(resp.text)
        
        return binaryId
        
def setFollowersUpdatedAt(entityId):
    entity.update_item(
        Key={
            'entityId': entityId
        },
        UpdateExpression='SET githubFollowersUpdatedAt = :val1',
        ExpressionAttributeValues={
            ':val1': str(datetime.datetime.now())
        }
    )
    
    
    
    
def getFollowers(entityId, githubUser):
    
            
    getNextApiPage
    

def getFollowerPage(url)
    resp = requests.get(restURL + '/users/' + githubUser + '/followers' + '?access_token=' + restToken)
    if resp.status_code != 200:
        # This means something went wrong.
        #raise ApiError('GET /me/ {}'.format(resp.status_code))
        print(resp.text)
        
    else:
        jsonResp = json.loads(resp.content)
        for follower in jsonResp:   
            githubLogin = follower["login"]
        
            print('login = ' + githubLogin)
            putEntity(githubLogin)

        
    
    
    
githubUser = 'rstoyanchev'
entityId = putEntity(githubUser)
getFollowers(entityId, githubUser)
setFollowersUpdatedAt(entityId)
    


